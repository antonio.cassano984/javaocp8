package cat.species;
import cat.BigCat;

public class Lynx extends BigCat {

  public void printFields() {
    System.out.println(this.name + " : " + this.hasFur);
  }

  public static void main(String[] args) {

    // in BigCat class
    BigCat cat = new BigCat();
    System.out.println(cat.name);  // public
    //System.out.println(cat.hasFur);  // protected
    //System.out.println(cat.hasPaws);  // default
    //System.out.println(cat.id);  // private

    // in Lynx class
    Lynx lynx = new Lynx();
    System.out.println(lynx.name);  // public
    System.out.println(lynx.hasFur);  // protected
    //System.out.println(lynx.hasPaws);  // default
    //System.out.println(lynx.id);  // private

    // in Lynx class
    BigCat lynx2 = new Lynx();
    System.out.println(lynx2.name);  // public
    //System.out.println(lynx2.hasFur);  // protected
    //System.out.println(lynx2.hasPaws);  // default
    //System.out.println(lynx2.id);  // private

  }
}